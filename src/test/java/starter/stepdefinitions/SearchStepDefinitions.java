package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.hamcrest.Matchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
       SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.body("title", hasItem(containsStringIgnoringCase("Mango"))));
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error",is(equalTo(true))));
    }

    @Then("he sees the results displayed for unauthorized access")
    public void heSeesTheResultsDisplayedForUnauthorizedUser() {
        restAssuredThat(response -> response.statusCode(401));
        restAssuredThat(response -> response.body("detail", is(equalTo("Not authenticated"))));
    }
    @Then("he sees the results displayed with {string}")
    public void heSeesTheResultsDisplayedForProvider(String providersList) {
        List<String> providerList= Arrays.asList(providersList.split(","));
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("provider",hasItem(anyOf((providerList).stream().map(Matchers::is).collect(Collectors.toList())))));
    }
    @Then("he sees the results displayed with productdetails")
    public void heSeesTheResultsDisplayedForTofu(DataTable productDetails) {
        List<String> list = new ArrayList<>(productDetails.asList(String.class));
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("$",hasItem(anyOf(hasKey(list)))));
    }
}
