Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios
  @SearchProductTest
  Scenario:HLS-001 Verify apple,mango and non-existing product response
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he doesn not see the results

  @SearchProductTest
  Scenario Outline:HLS-002 Verify all products have provider list as Vomar or AH  or Coop
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed with "<provider>"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed with "<provider>"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then he sees the results displayed with "<provider>"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
    Then he sees the results displayed with "<provider>"
    Examples:
      | provider      |
      | Vomar,AH,Coop |

  @SearchProductTest
  Scenario:HLS-003 Verify 401 Authentication Error with no product value in path param
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test"
    Then he sees the results displayed for unauthorized access
