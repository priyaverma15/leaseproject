# LeasePlan Assignment

In the assignment following points are covered

- Automated the API endpoints with one positive and one negative scenario **[Explained All points below]**
- Fixed the existing failed test case **[Explained All points below]**
- Refactored the existing code **[Explained All points below]**
- Updated/added the required configuration to run the project **[Explained All points below]**
- Integrated with Gitlab CI **[Explained All points below]**

## Project Gitlab 

PROJECT_CHECKOUT_FOLDER - https://gitlab.com/priyaverma15/leaseproject.git

Move to the base of the leaseProject  i.e, `${PROJECT_CHECKOUT_FOLDER}\leaseProject `
  
  git clone https://gitlab.com/priyaverma15/leaseproject.git

  cd ${PROJECT_CHECKOUT_FOLDER}\leaseProject

  git pull

## Structure

  `src/main/java` for all java classes.
  
  `src/test/java/starter` Test runner class
  
  `src/test/java/starter/stepdefinitions` Feature's Step Definitions
  
  `src/test/resources/feature` Feature files 

  `src/test/resources/feature/search`Feature file subdirectories

## Build

A prerequisite to build the project is `gradle`.

    ./gradlew build -x test  # build without tests

    ./gradlew --build-cache TestRunner aggregate # build and run tests and generate aggregated index.html report


## Run Cucumber Tests

To run all tests in local and fetch Serenity test report`gradle`.

    ./gradlew --build-cache TestRunner aggregate

    - target/site/serenity -Serenity report path

    - target/site/serenity/index.html - By using aggregate index.html will be genareted with all test cases

## Automated the API endpoint with one positive and one negative scenario(HLS - High Level Scenario)

HLS-002 -Verify all products have provider list as Vomar or AH  or Coop

```java   
| I have products "apple", "mango", "tofu", "water".    |
| I hit API request for "apple", "mango", "tofu", "water" products.    |
| I verify API response of all products have Product Providers as Vomar or AH or Coop.   |
```

HLS-003 -Verify 401 Authentication Error with no product value in path param

```java   
| I hit request with Blank/NULL value path param in products field    |
| I verify 401 Unauthorized access   |
```

### Fixed the existing failed test case

HLS-001 Verify apple,mango and non-existing product response
```java
BeforeFix
        restAssuredThat(response -> response.body("error", contains("True")));
AfterFix
        restAssuredThat(response -> response.body("detail.error",is(equalTo(true))));

        ###RCA FIX1 - Response body path was incorrect and null response was returning
        ###RCA FIX2 - Response body error value was returning boolean value 

```
```java
BeforeFix
        restAssuredThat(response -> response.body("title", contains("mango")));
AfterFix
        restAssuredThat(response -> response.body("title", hasItem(containsStringIgnoringCase("Mango"))));

        ###RCA FIX1 - Response body title retuens List<String> so used hasItem to match the string
        ###RCA FIX2 - Response body title returns both lower/upper cases for string so used containsStringIgnoringCase to ignore the case sensitivity 

```
## Refactored the existing code

Removed Unused class CarsAPI since its not been used anywhere

    java/starter/stepdefinitions/CarsAPI.java

Removed use of CarsAPI class from SearchStepDefinitions
```java
          //@Steps
          //public CarsAPI carsAPI;
```          
         
## Updated/added the required configuration to run the project

1. **Update in build.gradle configuration**
Added below Serenity RestAssured dependency for using the SerenityRest class methods
```java
      "net.serenity-bdd:serenity-rest-assured:${serenityCoreVersion}",
```          
Added TestRunner task in build.gradle which can be executed from CI validate stage
```java
            task TestRunner(type: Test) {
            systemProperty "cucumber.options", findProperty("cucumber.options")
            scanForTestClasses = false //Classes which match the include pattern are executed but not scanned
            testLogging.showStandardStreams = false // show standard out and standard error of the test JVM(s) on the console
            include "**/**TestRunner.class" // explicitly include tests which we want to run
            }
```  
2. Updated the gradle version 
   **Issue with RCA**  
       After integrating the gitlab CI Gradle kotlin was conflicting with Serenity version and Serenity reports were not generating

```java
           * Failed to generate report for net.thucydides.core.reports.html.RequirementsTypeReportingTask@46bf13a0 - java.util.concurrent.ExecutionException: java.lang.NoSuchMethodError: 'java.lang.Comparable kotlin.collections.CollectionsKt.minOrNull(java.lang.Iterable)'
        net.serenitybdd.reports.model.DurationsKt.startToFinishTimeIn(Durations.kt:45)
```
**Fix**
   Updated the gradle version from 4.9 to the latest 6.8.3

   Gradle property path - gradle/wrapper/gradle-wrapper.properties
   `distributionUrl=https\://services.gradle.org/distributions/gradle-6.8.3-bin.zip`

## Integrated with Gitlab CI

To open Serenity Report in Gitlab CI job 
- Click browse on the job to access the Serenity report
- Go to target/site/serenity/index.html


**.gitlab-ci.yml** 

image: adoptopenjdk:11-jdk-hotspot

stages:
- validate
  cache: &cache_template
  key:
  prefix: gradle
  files:
  - build.gradle
  - gradle/wrapper/gradle-wrapper.properties
    policy: pull
    paths:
  - .gradle/wrapper
  - .gradle/caches
    automation:
    stage: validate

script:
- chmod +x gradlew 
- ./gradlew --build-cache TestRunner aggregate
-Pcucumber.options="src/test/resources/features --tags '@SearchProductTest'"

artifacts:
when: always
paths:
- target/site/serenity



